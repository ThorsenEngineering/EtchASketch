from tkinter import *
root = Tk()
class DrawingBoard:

    x1, y1 = 40, 40
    frame1 = Canvas(root, width=200, height=200, bg="blue")    
    def  draw_oval(self,colour):
        self.frame1.create_oval(self.x1-1, self.y1-1, self.x1+1, self.y1+1, outline=colour, 
            fill=colour, width=5)

    def up(self,event):
       self.draw_oval("black")
       self.y1 = self.y1 - 5
       self.draw_oval("grey")  

    def down(self,event):
       self.draw_oval("black")
       self.y1 = self.y1 + 5
       self.draw_oval("grey")

    def left(self,event):
       self.draw_oval("black")
       self.x1 = self.x1 - 5
       self.draw_oval("grey")
       
    def right(self,event):
       self.draw_oval("black")
       self.x1 = self.x1 + 5
       self.draw_oval("grey")
    

drawing_board = DrawingBoard()
drawing_board.draw_oval("grey")
drawing_board.frame1.focus_set()
#frame1.create_line(15, 25, 200, 25)
drawing_board.frame1.bind("<Up>", drawing_board.up)
drawing_board.frame1.bind("<Down>", drawing_board.down)
drawing_board.frame1.bind("<Left>", drawing_board.left)
drawing_board.frame1.bind("<Right>", drawing_board.right)

drawing_board.frame1.pack()





root.mainloop()